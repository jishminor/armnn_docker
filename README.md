# ArmNN Docker
This repository provides native arm64 Dockerfiles to build ArmNN v21.05 and v21.08 with ONNX and TFLite parsing support along with TFLite delegate support.
You can build ArmNN with both TFLite v2.3.1 (officially supported) and v2.4.1 (supported through patches)

Patches for TFLite v2.4.1 build found here: https://gitlab.com/arm-hpc/packages/-/wikis/packages/tensorflow-lite



## Build Instructions Manual
Build base image containing common ArmNN dependencies
```
docker build -f Dockerfile.arm64-base -t armnn-base .
```

Now export the version of ArmNN you would like to build for:
```
export ARMNN_VERSION=v21.05
```
or
```
export ARMNN_VERSION=v21.08
```

Finally export the version of TFLite you would like to build for:
```
export TFLITE_VERSION=v2.3.1
```
or
```
export TFLITE_VERSION=v2.4.1
```

### Build Release
To build the release version:
```
docker build -f Dockerfile.arm64-tflite${TFLITE_VERSION} --build-arg ARMNN_TAG=${ARMNN_VERSION} -t armnn_${ARMNN_VERSION}_tflite_${TFLITE_VERSION} .
```

### Build Debug
To build the debug version:
```
docker build -f Dockerfile.arm64-tflite${TFLITE_VERSION} --build-arg ARMNN_TAG=${ARMNN_VERSION} --build-arg BUILD_TYPE=DEBUG -t armnn_${ARMNN_VERSION}_tflite_${TFLITE_VERSION}_debug .
```

### Build without RUY
To disable RUY in TFLite build, add flag below to the above commands:
```
--build-arg ENABLE_RUY=OFF
```

## Benchmarking
To benchmark models using ArmNN or TFLite tools, first run the generated container from above, optionally mounting in model files using docker volumes:
```
docker run --rm -it -v <model_dir_host>:/models armnn_${ARMNN_VERSION}_tflite_${TFLITE_VERSION}
```

Then within the container run a variant of the following command to suit your needs:
```bash
# Benchmark tflite default runtime
./benchmark_model --graph=/models/inceptionv3/1/model.tflite --num_threads=4

# Benchmark tflite xnnpack delegate runtime
./benchmark_model --graph=/models/inceptionv3/1/model.tflite --num_threads=4 --use_xnnpack=true

# Benchmark tflite armnn delegate runtime
./benchmark_model --graph=/models/inceptionv3/1/model.tflite --external_delegate_path=/root/armnn-devenv/armnn/build/delegate/libarmnnDelegate.so --external_delegate_options="backends:CpuAcc;number-of-threads:4"

# Run tflite model using ArmNN TFlite parser
./ExecuteNetwork -c CpuAcc -f tflite-binary -m /models/inceptionv3/1/model.tflite -i input -o InceptionV3/Predictions/Reshape_1

# Run tflite model using ArmNN delegate runtime
./ExecuteNetwork -c CpuAcc -f tflite-binary -m /models/inceptionv3/1/model.tflite -i input -o InceptionV3/Predictions/Reshape_1 -T delegate -s 1,299,299,3
```