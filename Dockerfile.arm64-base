FROM ubuntu:20.04
ENV TERM linux
ENV DEBIAN_FRONTEND noninteractive

# Forward system proxy setting
# ARG proxy
# ENV http_proxy $proxy
# ENV https_proxy $proxy

# Basic apt update
RUN apt-get update && apt-get install -y --no-install-recommends locales ca-certificates

# Set the locale to en_US.UTF-8, because the Yocto build fails without any locale set.
RUN locale-gen en_US.UTF-8 && update-locale LC_ALL=en_US.UTF-8 LANG=en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LC_ALL en_US.UTF-8

# Again, off the certificare
RUN echo "check_certificate = off" >> ~/.wgetrc
RUN echo "[global] \n\
    trusted-host = pypi.python.org \n \
    \t               pypi.org \n \
    \t              files.pythonhosted.org" >> /etc/pip.conf

# Get basic packages
RUN apt-get update && apt-get install -y \
    apparmor \
    aufs-tools \
    automake \
    bash-completion \
    build-essential \
    cmake \
    curl \
    dpkg-sig \
    g++ \
    gcc \
    git \
    iptables \
    jq \
    libapparmor-dev \
    libc6-dev \
    libcap-dev \
    libsystemd-dev \
    libyaml-dev \
    mercurial \
    net-tools \
    parallel \
    pkg-config \
    python3-dev \
    python3-mock \
    python3-pip \
    python3-numpy \
    python3-setuptools \
    python3-websocket \
    golang-go \
    iproute2 \
    iputils-ping \
    vim-common \
    vim \
    wget \
    libtool \
    unzip \
    scons \
    autoconf \
    libtool \
    default-jdk \
    zip \
    gdb \
    cmake && \
    rm -rf /var/lib/apt/lists/*

# Build and install Google's Protobuf library
# Download and Extract
RUN mkdir -p $HOME/google && \
    cd $HOME/google && \
    wget https://github.com/protocolbuffers/protobuf/releases/download/v3.12.0/protobuf-all-3.12.0.tar.gz && \
    tar -zxvf protobuf-all-3.12.0.tar.gz

# Build a native protobuf version
RUN cd $HOME/google/protobuf-3.12.0 && \
    mkdir arm64_build && cd arm64_build && \
    ../configure --prefix=$HOME/armnn-devenv/google/arm64_pb_install && \
    make install -j$(nproc)

# Build Boost library for arm64
RUN cd $HOME && wget http://downloads.sourceforge.net/project/boost/boost/1.64.0/boost_1_64_0.tar.gz && \
    tar xfz boost_1_64_0.tar.gz && \
    rm boost_1_64_0.tar.gz && \
    cd boost_1_64_0 && \
    ./bootstrap.sh --prefix=$HOME/armnn-devenv/boost_arm64_install && \
    ./b2 install link=static cxxflags=-fPIC --with-filesystem --with-test --with-log --with-program_options -j$(nproc)

# Download Flatbuffer
RUN cd $HOME/armnn-devenv && \
    wget -O flatbuffers-1.12.0.tar.gz https://github.com/google/flatbuffers/archive/v1.12.0.tar.gz && \
    tar xf flatbuffers-1.12.0.tar.gz && \
    cd flatbuffers-1.12.0 && \
    rm -f CMakeCache.txt

# Build flatbuffers
RUN cd $HOME/armnn-devenv && cd flatbuffers-1.12.0 && \
    mkdir build && \
    cd build && \
    CXXFLAGS="-fPIC" cmake .. -DFLATBUFFERS_BUILD_FLATC=1 \
    -DCMAKE_INSTALL_PREFIX:PATH=$HOME/armnn-devenv/flatbuffers \
    -DFLATBUFFERS_BUILD_TESTS=0 && \
    make all install -j$(nproc)

# Build onnx
RUN cd $HOME/armnn-devenv && git clone --depth 1 https://github.com/onnx/onnx.git && \
    cd onnx && \
    git fetch https://github.com/onnx/onnx.git 553df22c67bee5f0fe6599cff60f1afc6748c635 && git checkout FETCH_HEAD && \
    LD_LIBRARY_PATH=$HOME/armnn-devenv/google/arm64_pb_install/lib:$LD_LIBRARY_PATH \
    $HOME/armnn-devenv/google/arm64_pb_install/bin/protoc \
    onnx/onnx.proto --proto_path=. --proto_path=../google/arm64_pb_install/include --cpp_out $HOME/armnn-devenv/onnx

# Build Bazel for TFLite v2.3.1 build
RUN cd $HOME/armnn-devenv && \
    wget -O bazel-3.1.0-dist.zip https://github.com/bazelbuild/bazel/releases/download/3.1.0/bazel-3.1.0-dist.zip && \
    unzip -d bazel bazel-3.1.0-dist.zip && \
    rm bazel-3.1.0-dist.zip && \
    cd bazel && \
    ln -s /usr/bin/python3 /usr/bin/python && \
    env EXTRA_BAZEL_ARGS="--host_javabase=@local_jdk//:jdk" bash ./compile.sh

ARG ARMNN_TAG=v21.08
ARG ACL_TAG=$ARMNN_TAG
ARG BUILD_TYPE=RELEASE
ENV ARMNN_TAG=${ARMNN_TAG}
ENV BUILD_TYPE=${BUILD_TYPE}

# Build Compute Library
RUN cd $HOME/armnn-devenv/ && git clone -b ${ACL_TAG} --depth 1 https://review.mlplatform.org/ml/ComputeLibrary && \
    cd ComputeLibrary && \
    if [ "$BUILD_TYPE" = "DEBUG" ]; then \
    ACL_DEBUG=1; \
    else \
    ACL_DEBUG=0; \
    fi && \
    scons Werror=0 build=native arch=arm64-v8a neon=1 opencl=1 embed_kernels=1 debug=${ACL_DEBUG} extra_cxx_flags="-fPIC" -j$(nproc) internal_only=0 examples=0

# Download ArmNN
RUN cd $HOME/armnn-devenv && \
    git clone -b ${ARMNN_TAG} --depth 1 https://github.com/ARM-software/armnn.git

# Build ethos n library and link backend into armnn source tree
RUN cd $HOME/armnn-devenv && \
    git clone -b $(echo ${ARMNN_TAG} | sed 's/v//') --depth 1 https://github.com/ARM-software/ethos-n-driver-stack && \
    cd ethos-n-driver-stack/driver && \
    scons -j$(nproc) && \
    mkdir -p $HOME/armnn-devenv/ethosn && \
    scons install_prefix=$HOME/armnn-devenv/ethosn install && \
    ln -s $HOME/armnn-devenv/ethos-n-driver-stack/armnn-ethos-n-backend $HOME/armnn-devenv/armnn/src/backends/ethos-n