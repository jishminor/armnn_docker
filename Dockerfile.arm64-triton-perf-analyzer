ARG UBUNTU_VERSION=20.04

FROM ubuntu:${UBUNTU_VERSION} as build

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update && \
    apt-get install -yqq --no-install-recommends \
    git \
    wget \
    gnupg2 \
    software-properties-common \
    curl \
    libcurl4-openssl-dev \
    libb64-dev \
    libssl-dev \
    zlib1g-dev \
    rapidjson-dev \
    build-essential && \
    wget -O - https://apt.kitware.com/keys/kitware-archive-latest.asc 2>/dev/null | \
    gpg --dearmor - |  \
    tee /etc/apt/trusted.gpg.d/kitware.gpg >/dev/null && \
    apt-add-repository 'deb https://apt.kitware.com/ubuntu/ focal main' && \
    apt-get update && \
    apt-get install -y --no-install-recommends \
    cmake-data=3.21.1-0kitware1ubuntu20.04.1 cmake=3.21.1-0kitware1ubuntu20.04.1

WORKDIR /opt
RUN git clone https://github.com/triton-inference-server/client && \
    cd client && \
    mkdir build && \
    cd build && \
    cmake .. \
    -DCMAKE_INSTALL_PREFIX=`pwd`/install \
    -DTRITON_ENABLE_CC_HTTP=ON \
    -DTRITON_ENABLE_CC_GRPC=ON \
    -DTRITON_ENABLE_PERF_ANALYZER=ON \
    -DTRITON_ENABLE_GPU=OFF \
    && \
    make -j$(nproc) cc-clients

FROM ubuntu:${UBUNTU_VERSION}

RUN apt-get update && \
    apt-get install -yqq --no-install-recommends \
    libb64-0d \
    libssl-dev \
    zlib1g

COPY --from=build /opt/client/build/install/bin/perf_* /usr/bin/
