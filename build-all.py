import argparse
import docker
import os
import re
from pathlib import Path
import subprocess
import sys


def pull(git_directory, remote_name="origin", branch="main"):
    subprocess.Popen(["git", "pull", remote_name, branch], cwd=git_directory)


def checkout_ref(git_directory, ref):
    """
    Update repo with latest changes based on ref type
    """
    fetch = subprocess.Popen(["git", "fetch"], cwd=git_directory)
    returncode = fetch.wait()
    if returncode != 0:
        print(f"Failed to fetch from triton remote")
        sys.exit(1)

    checkout = subprocess.Popen(["git", "checkout", ref], cwd=git_directory)
    returncode = checkout.wait()
    if returncode != 0:
        print(f"Failed to checkout ref: {ref} from triton repo")
        sys.exit(1)


if __name__ == "__main__":

    # Handle triton subrepo
    Path("triton-build").mkdir(parents=True, exist_ok=True)

    # First checkout triton and armnn_tflite_backend repos
    if not Path("triton-build/server").is_dir():
        subprocess.Popen(
            ["git", "clone", "https://github.com/triton-inference-server/server"],
            cwd="triton-build",
        )

    # Checkout triton fork for special builds
    if not Path("triton-build/server-fork").is_dir():
        subprocess.Popen(
            ["git", "clone", "-b", "feature/ethosn", "https://github.com/jishminor/server", "server-fork"],
            cwd="triton-build",
        )

    # Handle armnn_tflite_backend subrepo
    Path("armnn-tflite-build").mkdir(parents=True, exist_ok=True)

    if not Path("armnn-tflite-build/armnn_tflite_backend").is_dir():
        subprocess.Popen(
            [
                "git",
                "clone",
                "https://gitlab.com/arm-research/smarter/armnn_tflite_backend",
            ],
            cwd="armnn-tflite-build",
        )

    parser = argparse.ArgumentParser()

    parser.add_argument(
        "--push", action="store_true", required=False, help="Push images to registry"
    )
    parser.add_argument(
        "--registry",
        type=str,
        required=False,
        help="Url of registry",
    )
    parser.add_argument(
        "--enable-debug-builds",
        action="store_true",
        required=False,
        help="Enable building debug builds for each container",
    )
    parser.add_argument(
        "--image-types",
        type=str,
        required=False,
        default=["armnn", "triton"],
        choices=["armnn", "triton", "perf_analyzer"],
        nargs="+",
        help="Image to build",
    )
    parser.add_argument(
        "--armnn-versions",
        type=str,
        required=False,
        default=["v21.05", "v21.08"],
        choices=["v21.05", "v21.08"],
        nargs="+",
        help="ArmNN tag associated with the build.",
    )
    parser.add_argument(
        "--armnn-tflite-backend-refs",
        type=str,
        required=False,
        default=["main"],
        nargs="+",
        help="ArmNN TFLite backend refs associated with the build.",
    )
    parser.add_argument(
        "--triton-refs",
        type=str,
        required=False,
        default=["r21.10"],
        choices=["main", "r21.09", "r21.10"],
        nargs="+",
        help="Triton tags associated with the build.",
    )
    parser.add_argument(
        "--tflite-versions",
        type=str,
        required=False,
        default=["v2.3.1", "v2.4.1"],
        choices=["v2.3.1", "v2.4.1"],
        nargs="+",
        help="TFLite tag associated with the build.",
    )
    parser.add_argument(
        "--build-ruy",
        type=str,
        required=False,
        default=["ON", "OFF"],
        choices=["ON", "OFF"],
        nargs="+",
        help="Whether to build ruy support into tflite",
    )
    parser.add_argument(
        "--build-mali",
        type=str,
        required=False,
        default=["OFF"],
        choices=["ON", "OFF"],
        nargs="+",
        help="Whether to build mali support into armnn tflite backend",
    )
    parser.add_argument(
        "--build-ethos",
        type=str,
        required=False,
        default=["OFF"],
        choices=["ON", "OFF"],
        nargs="+",
        help="Whether to build ethosn support into armnn tflite backend",
    )
    parser.add_argument(
        "--build-types",
        type=str,
        required=False,
        default=["RELEASE"],
        choices=["RELEASE", "DEBUG"],
        nargs="+",
        help="Build types for output libraries",
    )
    parser.add_argument(
        "--content",
        type=str,
        required=False,
        default=["final"],
        choices=["builder", "final"],
        nargs="+",
        help="Build types for armnn, either builder or just the final binaries",
    )
    args = parser.parse_args()

    if args.push and not (args.registry):
        parser.error("--push and --registry must be given together")

    client = docker.from_env(timeout=3600)

    if "armnn" in args.image_types:

        # Build all images as per args
        for armnn_version in args.armnn_versions:
            for tflite_version in args.tflite_versions:
                for build in args.build_types:
                    for ruy in args.build_ruy:
                        for content in args.content:

                            dockerfileargmap = {
                                "ARMNN_TAG": armnn_version,
                                "BUILD_TYPE": build,
                                "ENABLE_RUY": ruy,
                            }

                            # First build armnn base image
                            print(f"Building base image: armnn-base")
                            client.images.build(
                                path=".", dockerfile="Dockerfile.arm64-base", tag="armnn-base", buildargs=dockerfileargmap,
                            )

                            image_tag = f"armnn_{armnn_version}_tflite_{tflite_version}_ruy_{ruy}_content_{content}_{build}".lower()
                            full_image_string = (
                                f"{args.registry}:{image_tag}"
                                if args.registry
                                else image_tag
                            )
                            print(f"Building image: {full_image_string}")
                            client.images.build(
                                path=".",
                                dockerfile=f"Dockerfile.arm64-tflite{tflite_version}",
                                buildargs=dockerfileargmap,
                                tag=full_image_string,
                                target=content,
                            )
                            if args.push:
                                print(f"Pushing image: {full_image_string}")
                                print(client.api.push(args.registry, tag=image_tag))

    if "triton" in args.image_types:
        for triton_ref in args.triton_refs:
            checkout_ref("triton-build/server", triton_ref)

            # Get the triton version from the tritonserver repo TRITON_VERSION file
            with open("triton-build/server/TRITON_VERSION", "r") as triton_version_file:
                triton_version = triton_version_file.read().splitlines()[0]
                print(f"Using triton_version {triton_version}")

            # Run build.py for each specified triton version to get a base image
            for mali in args.build_mali:
                for ethos in args.build_ethos:
                    triton_image_tag = f"tritonserver_{triton_version}_mali_{mali}{'_ethos_on' if ethos == 'ON' else ''}".lower()
                    try:
                        client.images.get(triton_image_tag)
                        print(f"Triton server base image {triton_image_tag} found")
                    except docker.errors.NotFound as e:
                        print(
                            f"Triton server base image {triton_image_tag} not found, building now..."
                        )
                        command = [
                            "./build.py",
                            "--cmake-dir=/workspace/build",
                            "--build-dir=/tmp/citritonbuild",
                            "--enable-logging",
                            "--enable-stats",
                            "--enable-tracing",
                            "--enable-metrics",
                            "--endpoint=http",
                            "--endpoint=grpc",
                            "--image=base,arm64v8/ubuntu:20.04",
                        ]
                        if mali == "ON":
                            command.append("--enable-mali-gpu")
                        if ethos == "ON":
                            command.append("--enable-ethos-npu --repo-tag backend:feature/ethosn --repo-tag core:feature/ethosn --repo-tag common:feature/ethosn")

                        print(f"Running build command: {command}")

                        build_proc = subprocess.Popen(
                            command,
                            cwd="triton-build/server" if ethos == "OFF" else "triton-build/server-fork",
                        )
                        returncode = build_proc.wait()

                        if returncode != 0:
                            print(
                                f"Failed to build base tritonserver image: {triton_image_tag}"
                            )
                            sys.exit(1)
                        else:
                            print(
                                f"Finished building base tritonserver image: {triton_image_tag}"
                            )

                    # Tag the built docker image with the triton verison
                    client.images.get("tritonserver:latest").tag(triton_image_tag)

                    # Build the final images
                    for armnn_tflite_ref in args.armnn_tflite_backend_refs:
                        checkout_ref(
                            "armnn-tflite-build/armnn_tflite_backend", armnn_tflite_ref
                        )

                        # Get the commit has corresponding to the ref for armnn_tflite_backend
                        commit_id = str(
                            subprocess.run(
                                ["git", "rev-parse", "--verify", "HEAD"],
                                cwd="armnn-tflite-build/armnn_tflite_backend",
                                check=True,
                                stdout=subprocess.PIPE,
                            ).stdout.decode("utf-8")
                        ).replace("\n", "")
                        print(commit_id)
                        for armnn_version in args.armnn_versions:
                            for build in args.build_types:
                                for ruy in args.build_ruy:
                                    dockerfileargmap = {
                                        "TRITON_IMAGE_NAME": triton_image_tag,
                                        "TRITON_REPO_TAG": triton_ref,
                                        "ARMNN_TFLITE_COMMIT_REF": commit_id,
                                        "ARMNN_TAG": armnn_version,
                                        "BUILD_TYPE": build,
                                        "BUILD_RUY": ruy,
                                        "BUILD_MALI": mali,
                                        "BUILD_ETHOS": ethos
                                    }
                                    image_tag = f"{triton_version}_armnn_{armnn_version}_ruy_{ruy}_gpu_{mali}_backendver_{armnn_tflite_ref.replace('/', '_')}_{build}".lower()
                                    full_image_string = (
                                        f"{args.registry}:{image_tag}"
                                        if args.registry
                                        else image_tag
                                    )
                                    print(f"Building image: {full_image_string}")
                                    try:
                                        client.images.build(
                                            path=".",
                                            dockerfile=f"Dockerfile.arm64-triton",
                                            buildargs=dockerfileargmap,
                                            tag=full_image_string,
                                        )
                                    except Exception as e:
                                        print(
                                            f"Failed building triton image with args: {dockerfileargmap} \nand error: {e}"
                                        )
                                        sys.exit(1)
                                    if args.push:
                                        print(f"Pushing image: {full_image_string}")
                                        print(client.api.push(args.registry, tag=image_tag))

    if "perf_analyzer" in args.image_types:
        for triton_ref in args.triton_refs:
            # Search for semantic version tag
            checkout_ref("triton-build/server", triton_ref)

            # Get the triton version from the tritonserver repo TRITON_VERSION file
            with open("triton-build/server/TRITON_VERSION", "r") as triton_version_file:
                triton_version = triton_version_file.read().splitlines()[0]
                print(f"Using triton_version {triton_version}")
            perf_analyzer_image_tag = f"{triton_version}_arm64"
            full_image_string = (
                f"{args.registry}:{perf_analyzer_image_tag}"
                if args.registry
                else perf_analyzer_image_tag
            )
            print(f"Building image: {full_image_string}")
            client.images.build(
                path=".",
                dockerfile=f"Dockerfile.arm64-triton-perf-analyzer",
                tag=full_image_string,
            )
            if args.push:
                print(f"Pushing image: {full_image_string}")
                print(client.api.push(args.registry, tag=perf_analyzer_image_tag))
